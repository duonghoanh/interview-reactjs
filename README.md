# React + Vite

This template provides a minimal setup to get React working in Vite with HMR and some ESLint rules.

step by step run

```bash
npm install
#install node module

npm run dev
#run the project
```

if you want clone git 
```bash
git clone https://gitlab.com/duonghoanh/interview-reactjs.git

npm install
#install node module

npm run dev
#run the project
```

if you need nextJS! i will update this link gitlab

```
###try it

https://gitlab.com/duonghoanh/nextjs-interview.git
```

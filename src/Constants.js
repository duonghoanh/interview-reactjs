export const DATA = [
  {
    title: "All",
    list: [
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/09/image-1.png",
        title: "TOI 3D Customize E-commerce",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/09/image.png",
        title: "E-learning – Internal training platform",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png",
        title: "Summer 21 Cosmetic E-commerce Platform",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png",
        title:
          "Kiva – Ambition to digital transformation in the brokerage assiduity",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/08/boxgo-thumbnail.png",
        title: "Boxgo – Professional Warehouse Management",
        path: "https://tekup.vn/en/projects",
      },
    ],
  },
  {
    title: "Manpower Supply",
    list: [
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/09/doitac-dsoft.jpg",
        title: "Dsoft",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/09/doitac-mitc.jpg",
        title: "MITTC",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/09/doitac-tego.jpg",
        title: "TEGO",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/09/doitac-mor.jpg ",
        title: "MOR",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/09/doitac-code-complete.jpg",
        title: "MOR",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/09/doitac-smart-dev.jpg",
        title: "SMART DEV",
        path: "https://tekup.vn/en/projects",
      },
    ],
  },
  {
    title: "Mobile App / Websites",
    list: [
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/08/boxgo-thumbnail.png",
        title: "Boxgo – Nền tảng quản lý kho chuyên nghiệp",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png",
        title: "Kiva – Tham vọng chuyển đổi số ngành môi giới",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png",
        title: "Website Thương Mại Điện Tử – Summer21",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/09/image.png",
        title: "E-learning – Nền tảng đào tạo nội bộ",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail: "https://tekup.vn/wp-content/uploads/2022/09/image-1.png",
        title: "TOI 3D Customize E-commerce",
        path: "https://tekup.vn/en/projects",
      },
    ],
  },
  {
    title: "UI/UX Design",
    list: [
      {
        thumbnail:"https://tekup.vn/wp-content/uploads/2022/08/boxgo-thumbnail.png",
        title: "Boxgo – Nền tảng quản lý kho chuyên nghiệp",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:"https://tekup.vn/wp-content/uploads/2022/08/kiva-thumbnail.png",
        title: "Kiva – Tham vọng chuyển đổi số ngành môi giới",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:
          "https://tekup.vn/wp-content/uploads/2022/08/summer21-thumbnail.png",
        title: "Website Thương Mại Điện Tử – Summer21",
        path: "https://tekup.vn/en/projects",
      },
      {
        thumbnail:"https://tekup.vn/wp-content/uploads/2022/09/image.png",
        title: "E-learning – Nền tảng đào tạo nội bộ",
        path: "https://tekup.vn/en/projects",
      },
    
    ],
  },
];

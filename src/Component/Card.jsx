import React from "react";
import "./style.css";

const Card = ({ props }) => {
  const { thumbnail, title, path } = props;
  return (
    <a href={path}>
      <div className="card">
        <div className="card__img">
          <img src={thumbnail} alt="img" />
        </div>
        <div className="card__title">
          <h6>{title}</h6>
          <span>
          &#62;
          </span>
        </div>
      </div>
    </a>
  );
};

export default Card;
